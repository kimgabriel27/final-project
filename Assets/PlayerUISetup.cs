﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class PlayerUISetup : MonoBehaviour
{
    public TMP_Text gunAmmoText;
    public TMP_Text gunNameText;
    public TMP_Text waveText;
    public TMP_Text killCount;
    public TMP_Text stateText;
    public Image healthBar;

    GameManager gameManager;
    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManager.instance;
    }

    // Update is called once per frame
    void Update()
    {
        //waveText.text = gameManager.GetCurrentWave().waveName;
    }
}

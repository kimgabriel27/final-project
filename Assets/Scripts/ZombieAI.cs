﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;
using Photon.Pun;
using Photon.Realtime;

public class ZombieAI : MonoBehaviourPunCallbacks, IPunObservable
{
    NavMeshAgent agent;

    [SerializeField] public List<GameObject> targets;
    [SerializeField] public GameObject playerTarget;
    [SerializeField] private Animator anim;

    private Vector3 syncPos = Vector3.zero;
    private Quaternion syncRot = Quaternion.identity;

    [Header("Zombie Stats")]
    [SerializeField] private float currentHP;
    [SerializeField] private float maxHp;
    [SerializeField] private float zombieDamage;
    [SerializeField] private Image healthBar;
    public float nearestDist = 2f;
    public bool isDead = false;

    bool isLateUpdating = false;

    private void Awake()
    {
        syncPos = transform.position;
        syncRot = transform.rotation;

    }
    // Start is called before the first frame update
    void Start()
    {
        agent = GetComponent<NavMeshAgent>();
        anim = GetComponent<Animator>();
        maxHp = 20;
        currentHP = maxHp;
        zombieDamage = 10;
        healthBar.fillAmount = currentHP / maxHp;
        targets = new List<GameObject>();

        if (PhotonNetwork.IsMasterClient)
        {
            //FindTarget();
            playerTarget = GameObject.FindGameObjectWithTag("Player");
            if (playerTarget.GetComponent<PlayerShooting>() == null)
                throw new System.Exception("Target doesnt have player shoooting");
        }
        else
        {
            agent.enabled = false;
        }
    }

    // Update is called once per frame
    void Update()
    {
        //if (targets.Count <= 0)
        //{
        //    return;
        //}
    
        //if (!PhotonNetwork.IsMasterClient)
        //{
        //     SyncTransform();
        //}
        //else
        //{

       
        //}
        FindTarget();
        Chase();
        //if (playerTarget == null) return;


        //   playerTarget = FindClosestTarget();

        // Seek(playerTarget.transform.position);
        //photonView.RPC("Seek", RpcTarget.AllBuffered, playerTarget.transform.position);

    }

    [PunRPC]
    void Seek(Vector3 location)
    {
        if (location == null) { return; }
        if (agent.enabled == false) { return; }
        //   Debug.Log(location);
        //  agent.SetDestination(location);
        agent.destination = location;
        float dist = Vector3.Distance(this.transform.position, location);
       // photonView.RPC("CheckDistance", RpcTarget.All, dist);

        //  Debug.Log(dist);
        // anim.SetFloat("TargetDistance", dist);
         CheckDistance(dist);
    }

    [PunRPC]
    void CheckDistance(float dist)
    {
        if (!photonView.IsMine) return;
        if (dist <= 1.8f)
        {
            anim.SetBool("IsAttacking", true);
            anim.SetBool("isRunning", false);
            agent.speed = 0;
        }
        else if (dist > 1.8 )
        {
            anim.SetBool("IsAttacking", false);
            anim.SetBool("isRunning", true);
            agent.speed = 3.5f;
        }

    }
    bool AnimatorIsPlaying()
    {
        return anim.GetCurrentAnimatorStateInfo(0).length > anim.GetCurrentAnimatorStateInfo(0).normalizedTime;
    }

    [PunRPC]
    void BroadcastDamagePlayer()
    {
        targets[0].SendMessage("TakeDamage", zombieDamage, SendMessageOptions.DontRequireReceiver);
    }
    public void EndAttack()
    {
        float dist = Vector3.Distance(targets[0].transform.position, this.transform.position);

        if (dist <= 1.8f)
        {
            Debug.Log("Hit Player" + targets[0].name);
            targets[0].SendMessage("TakeDamage", zombieDamage, SendMessageOptions.DontRequireReceiver);

        }

        if(targets[0].GetComponent<PlayerShooting>().isDead)
        {
            targets.Remove(targets[0]);
        }
    }

    [PunRPC]
    public void TakeDamage(float damage, string playerOwner)
    {
        RPCBroadcastDamage(damage);

        if (currentHP <= 0)
        {
            //  anim.SetBool("isDead", true);
            EventManager.StartEnemyDeathEvent(playerOwner);
            RPCBroadcastDead();
            photonView.RPC("RPCBroadcastDead", RpcTarget.All);
           
        }
    }

    public float SetDamage(float damage)
    {

        return zombieDamage = damage;
    }

    public float SetHealth(float health)
    {
        maxHp = health;
        currentHP = maxHp;
        healthBar.fillAmount = currentHP / maxHp;
        return maxHp;
    }

    public float GetDamage()
    {
        return zombieDamage;
    }

    public float GetMaxHealth()
    {
        return maxHp;
    }

    public GameObject FindClosestTarget()
    {
        if (targets.Count <= 1)
        {
            return targets[0].GetComponent<PlayerSetup>().transform.Find("Player").gameObject;
        }
        float currentDist;
        float closestDist = Mathf.Infinity;
        GameObject closeGameObject = null;
        foreach (GameObject a in targets)
        {
            currentDist = Vector3.Distance(transform.position, a.transform.position);
            if (currentDist <= nearestDist)
            {
                closestDist = currentDist;
                closeGameObject = a;
            }
        }

        return closeGameObject;
    }

    void FindTarget()
    {
  
        foreach (var item in GameObject.FindGameObjectsWithTag("Player"))
        {
            if (!targets.Contains(item.gameObject))
            {
                targets.Add(item.gameObject);
            }
        }



    }

    public void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (stream.IsWriting)
        {
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        }
        else
        {
            syncPos = (Vector3)stream.ReceiveNext();
            syncRot = (Quaternion)stream.ReceiveNext();
        }
    }

    void SyncTransform()
    {
        transform.position = Vector3.Lerp(transform.position, syncPos, 0.1f);
        transform.rotation = Quaternion.Lerp(transform.rotation, syncRot, 0.1f);
    }

    [PunRPC]
    void RPCBroadcastDead()
    {
        isDead = true;
        agent.isStopped = true;
        anim.SetBool("isDead", true);
        anim.SetBool("isRunning", false);
        GameManager.instance.zombiesSpawned--;
        Destroy(this.gameObject, anim.GetCurrentAnimatorStateInfo(0).length);
       //PhotonNetwork.Destroy(gameObject.GetPhotonView, anim.GetCurrentAnimatorStateInfo(0).length);
    }

    [PunRPC]
    void RPCBroadcastDamage(float damage)
    {
        currentHP -= damage;
        healthBar.fillAmount = currentHP / maxHp;
    }

    IEnumerator CoLateUpdateDestination(float latency)
    {
        isLateUpdating = true;
        yield return new WaitForSeconds(latency);
        //   agent.destination = playerTarget.transform.position;
        Seek(playerTarget.transform.position);
       // photonView.RPC("Seek", RpcTarget.All, playerTarget.transform.position);
        isLateUpdating = false;
    }

    void Chase()
    {
        if (isDead) return;
        else if (playerTarget.GetComponent<PlayerShooting>().isDead) return;
        GameObject closestTarget = GetClosestTarget();

        if(closestTarget != playerTarget)
        {   
            playerTarget = closestTarget;
        }
       
        float dist = GetDistanceFromTarget(playerTarget);

        Seek(playerTarget.transform.position);
        if (dist >= 5)
        {
            if (!isLateUpdating)
            {
                StartCoroutine(CoLateUpdateDestination(0.1f));
            }
        }
        else
        {
            if (dist <= 10)
            {
                StartCoroutine(CoLateUpdateDestination(0.5f));
            }
            else if (dist <= 15)
            {
                StartCoroutine(CoLateUpdateDestination(1f));
            }
            else
            {
                StartCoroutine(CoLateUpdateDestination(2f));
            }
        }
    }

    float GetDistanceFromTarget(GameObject player)
    {
        return Vector3.Distance(player.transform.position, transform.position);
    }

    GameObject GetClosestTarget()
    {
        if (targets.Count <= 1) return targets[0].gameObject;


        GameObject closestTarget = targets[0].gameObject;
        float minDist = Mathf.Infinity;

        for (int i = 0; i < targets.Count; i++)
        {
            float dist = GetDistanceFromTarget(targets[i].gameObject);
            if(dist < minDist)
            {
                minDist = dist;
                closestTarget = targets[i].gameObject;
            }
        }

        return closestTarget;
    }

    
   
}

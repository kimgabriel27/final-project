﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Photon.Pun;
using TMPro;

public class PlayerSetup : MonoBehaviourPunCallbacks
{
    GameManager gameManager;

    public GameObject playerUIPrefab;
    public GameObject mainPlayer;
    public GameObject playerCamera;
    public Camera camera;

    public TMP_Text nameTxt;


    // Start is called before the first frame update
    void Start()
    {
        gameManager = GameManager.instance;
        nameTxt.text = photonView.Owner.NickName;

        GetComponent<PlayerShooting>().enabled = photonView.IsMine;
        GetComponent<PlayerMovement>().enabled = photonView.IsMine;
        GetComponent<InputHandler>().enabled = photonView.IsMine;
    
        if(photonView.IsMine)
        {
            GameObject UI = Instantiate(playerUIPrefab);
            GameObject camera = Instantiate(playerCamera);
            camera.GetComponent<PlayerCamera>().m_Target = this.transform;
            GetComponent<PlayerMovement>().camera = camera.GetComponent<Camera>();
            GetComponent<PlayerShooting>().waveText = UI.GetComponent<PlayerUISetup>().waveText;
            GetComponent<PlayerShooting>().gunAmmoText = UI.GetComponent<PlayerUISetup>().gunAmmoText;
            GetComponent<PlayerShooting>().gunNameText = UI.GetComponent<PlayerUISetup>().gunNameText;
            GetComponent<PlayerShooting>().killCountText = UI.GetComponent<PlayerUISetup>().killCount;
            GetComponent<PlayerShooting>().stateText = UI.GetComponent<PlayerUISetup>().stateText;
            GetComponent<PlayerShooting>().stateText.text = "";
            GetComponent<PlayerShooting>().healthBar = UI.GetComponent<PlayerUISetup>().healthBar;
            GetComponent<PlayerShooting>().SetupHealth();
        }

        //mainPlayer.GetComponent<PlayerShooting>().enabled = photonView.IsMine;
        //mainPlayer.GetComponent<PlayerMovement>().enabled = photonView.IsMine;
        //mainPlayer.GetComponent<InputHandler>().enabled = photonView.IsMine;
        //gameObject.transform.Find("Main Camera").gameObject.SetActive(photonView.IsMine);
        //gameObject.transform.Find("Main Camera").GetComponent<PlayerCamera>().enabled = photonView.IsMine;
        //if(photonView.IsMine)
        //{
        //    mainPlayer.SetActive(photonView.IsMine);
        //    GameObject UI = Instantiate(playerUIPrefab);
        //    PlayerUISetup playerUISetup = UI.GetComponent<PlayerUISetup>();
        //    // gameManager.WaveText = UI.transform.Find("WaveText").transform.gameObject.GetComponent<TMP_Text>();
        //    mainPlayer.GetComponent<PlayerShooting>().gunAmmoText = playerUISetup.gunAmmoText;
        //    mainPlayer.GetComponent<PlayerShooting>().gunNameText = playerUISetup.gunNameText;
        //    mainPlayer.GetComponent<PlayerShooting>().killCountText = playerUISetup.killCount;
        //    mainPlayer.GetComponent<PlayerShooting>().playerUI = UI;
        //    mainPlayer.GetComponent<PlayerShooting>().stateText = playerUISetup.stateText;
        //    mainPlayer.GetComponent<PlayerShooting>().stateText.text = "";
        //    mainPlayer.GetComponent<PlayerShooting>().healthBar = playerUISetup.healthBar;
        //    mainPlayer.GetComponent<PlayerShooting>().SetupHealth();

        //}






    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using Photon.Realtime;
using TMPro;

[System.Serializable]
public class Wave
{
    public string waveName;
    public int noOfEnemies;
    public GameObject[] typeOfEnemies;
    public float spawnInterval;
}

public class GameManager : MonoBehaviourPunCallbacks
{
    public static GameManager instance = null;

    [SerializeField] private int zombieCount;

    [SerializeField] float countdown = 5f;


    [Header("Wave Attributes")]
    [SerializeField] Transform[] spawnPoints;
    [SerializeField] private Wave[] waves;
    public Wave currentWave;
    private int currentWaveNumber;
    bool canSpawn = true;
    private float nextSpawnTime;

    public GameObject ghoulPrefab;
    public int maxZombieSpawns = 20;
    public int zombiesSpawned = 0;
    public float upgradeDuration = 20f;
    public float additionalHP = 0;
    public float additionalDamage = 0f;
    public float spawnInterval = 1.5f;
    private IEnumerator spawningEnemies;
    private IEnumerator upgradingEnemies;

    [Header("UI")]
    public TMP_Text WaveText;
    public GameObject[] finalKillsUI;
 
    [Header("Player Related variables")]
    public GameObject playerPrefab;
    public Transform[] playerSpawnPoints;
    public List<GameObject> playersInGame = new List<GameObject>();


    public int playersAlive = 0;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != null)
        {
            Destroy(gameObject);
        }
        DontDestroyOnLoad(gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {
        if(PhotonNetwork.IsConnectedAndReady)
        {
            int rand = Random.Range(0, playerSpawnPoints.Length);
            GameObject g = PhotonNetwork.Instantiate(playerPrefab.name, playerSpawnPoints[rand].position, Quaternion.identity);
            playersInGame.Add(g);
            playersAlive++;
        }

   

        foreach (GameObject item in finalKillsUI)
        {
            item.SetActive(false);
        }

        
        if (!PhotonNetwork.IsMasterClient) return;
    
        spawningEnemies = WaveSpawning();
        upgradingEnemies = EnhanceZombie();

        StartCoroutine(spawningEnemies);
        StartCoroutine(upgradingEnemies);
        

    }

    // Update is called once per frame
 


    void SpawnWave()
    {
        if (canSpawn && nextSpawnTime < Time.time)
        {
            WaveText.text = currentWave.waveName;
            GameObject randomEnemy = currentWave.typeOfEnemies[Random.Range(0, currentWave.typeOfEnemies.Length)];
            Transform randomPoint = spawnPoints[Random.Range(0, spawnPoints.Length)];
            // Instantiate(randomEnemy, randomPoint.position, Quaternion.identity);
           
            PhotonNetwork.Instantiate(randomEnemy.name, randomPoint.position, Quaternion.identity);
            currentWave.noOfEnemies--;
         
            randomEnemy.GetComponent<ZombieAI>().SetDamage(currentWaveNumber * randomEnemy.GetComponent<ZombieAI>().GetDamage());
            randomEnemy.GetComponent<ZombieAI>().SetHealth(currentWaveNumber * randomEnemy.GetComponent<ZombieAI>().GetMaxHealth());
            nextSpawnTime = Time.time + currentWave.spawnInterval;
            if (currentWave.noOfEnemies == 0)
            {
                canSpawn = false;

            }
        }
    }

   

    public Wave GetCurrentWave()
    {
        return currentWave;
    }

    public void GameOver()
    {
        StopCoroutine(spawningEnemies);
        StopCoroutine(upgradingEnemies);

        for (int i = 0; i < PhotonNetwork.CurrentRoom.PlayerCount; i++)
        {
            finalKillsUI[i].SetActive(true);
        }
    }

    IEnumerator WaveSpawning()
    {
       
        while(true)
        {
          

            for (int i = 0; i < spawnPoints.Length; i++)
            {

                if (zombiesSpawned >= maxZombieSpawns) continue;
                GameObject randomEnemy = PhotonNetwork.Instantiate(ghoulPrefab.name, spawnPoints[i].position, Quaternion.identity);
                randomEnemy.GetComponent<ZombieAI>().SetDamage(additionalHP + randomEnemy.GetComponent<ZombieAI>().GetDamage());
                randomEnemy.GetComponent<ZombieAI>().SetHealth(additionalDamage + randomEnemy.GetComponent<ZombieAI>().GetMaxHealth());
                zombiesSpawned++;
             
            }

            yield return new WaitForSeconds(1.5f);





        }
       // Player[] players = GameObject.FindObjectsOfType<Player>();

    }

    IEnumerator EnhanceZombie()
    {
        while(true)
        {
            yield return new WaitForSeconds(upgradeDuration);

            Debug.Log("Upgrading Enemy...");

            additionalDamage += 5f;
            additionalHP += 20f;
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using Photon.Pun;
using Photon.Realtime;
using ExitGames.Client.Photon;


[System.Serializable]
public class Gun
{
    public string gunName;
    public int ammo;
    public float fireRate;
    public float damage;
}
public class PlayerShooting : MonoBehaviourPunCallbacks
{
    public enum RaiseEventCode
    {
        DeadPlayerEventCode = 0
    }

    [SerializeField] public int killCount;
    [Header("Gun")]
    [SerializeField] private Gun[] guns;
    [SerializeField] private GameObject bulletPrefab;
    [SerializeField] private Transform gunSpawnPoint;
    [SerializeField] public float currentHp;
    [SerializeField] public float maxHp;

    [Header("UI")]
    public GameObject playerUI;
    public Image healthBar;
    public Gradient gradient; 
    public TMP_Text gunNameText;
    public TMP_Text gunAmmoText;
    public TMP_Text killCountText;
    public TMP_Text stateText;
    public TMP_Text waveText;
    public float bulletSpeed;
    public Gun currentGun;

    [Header("Player Reference")]
    PlayerMovement playerMovement;
    bool canShoot = true;
    public bool isDead = false;
    float fireTimer;

    int deathOrder = 0;

    private void OnEnable()
    {
        PhotonNetwork.NetworkingClient.EventReceived += OnDieEvent;
    }

    private void OnDisable()
    {
        PhotonNetwork.NetworkingClient.EventReceived -= OnDieEvent;
    }


    private void Start()
    {
        currentGun = guns[1];
        currentHp = maxHp;
     //   gunNameText =  playerUI.transform.Find("GunNameText").GetComponent<TMP_Text>();
       // gunAmmoText = playerUI.transform.Find("GunAmmoText").GetComponent<TMP_Text>();
        playerMovement = GetComponent<PlayerMovement>();
        EventManager.OnEnemyDeathEvent += AddKillCount;
        EventManager.OnWaveUpdate += WaveUpdate;

   //     SetupHealth();

    }

    private void Update()
    {
      //   waveText.text = GameManager.instance.GetCurrentWave().waveName;
        fireTimer += Time.deltaTime;
        if (Input.GetMouseButtonDown(0) && canShoot && fireTimer > currentGun.fireRate )
        {
            fireTimer = 0;
            photonView.RPC("ShootBullet", RpcTarget.AllBuffered);
           // ShootBullet();
           
          
        }
    }


    public void TakeDamage(float damage)
    {
        if (photonView.IsMine)
        {
            photonView.RPC("BroadcastDamage", RpcTarget.All, damage);
        }
     

    }

    [PunRPC]
    public void  BroadcastDamage(float damage)
    {
        if (!photonView.IsMine) return;
        currentHp -= damage;

        healthBar.fillAmount = currentHp / maxHp;

        healthBar.color = gradient.Evaluate(healthBar.fillAmount);


        if (currentHp <= 0)
        {
            photonView.RPC("Die", RpcTarget.All);
            //Die();
        }
    }

    [PunRPC]
    public void ShootBullet()
    {
    
        GameObject b = Instantiate(bulletPrefab);
        b.transform.position = gunSpawnPoint.transform.position;
        Vector3 rotation = b.transform.rotation.eulerAngles;

        b.transform.rotation = Quaternion.Euler(rotation.x, transform.eulerAngles.y, rotation.z);
        b.GetComponent<BulletScript>().damage = currentGun.damage;
        b.GetComponent<BulletScript>().playerOwner = photonView.Owner.NickName;
        b.GetComponent<Rigidbody>().AddForce(gunSpawnPoint.forward * bulletSpeed, ForceMode.Impulse);
        Destroy(b, 2);
        currentGun.ammo--;

        if (!photonView.IsMine) return;
        gunNameText.text = currentGun.gunName;
        //change to Pistol if other guns are empty  
        if (currentGun.ammo <= 0)
        {
            currentGun = guns[0];
        }

        if (currentGun.gunName == "Pistol")
        {
            gunAmmoText.text = "∞";
        }
        else
        {
            gunAmmoText.text = currentGun.ammo.ToString();
        }
    }

    public void SetupHealth()
    {
        healthBar.fillAmount = 1;
    //    healthBar.fillAmount = currentHp / maxHp;
        Debug.Log(currentHp / maxHp);
        healthBar.color = gradient.Evaluate(1f);
    }

    void AddKillCount(string name)
    {
        if (photonView.Owner.NickName != name)
            return;
        if(photonView.Owner.NickName == name)
            killCount++;
       
        killCountText.text = killCount.ToString();
    }

    void WaveUpdate(string waveName)
    {
        waveText.text = waveName;
    }
    
    [PunRPC]
    void Die()
    {
        if(photonView.IsMine && currentHp <= 0 && !isDead )
        {
            Debug.Log("Player Death Event");
            isDead = true;
            playerMovement.enabled = false;
            canShoot = false;
            
            string name = photonView.Owner.NickName;
            deathOrder++;
            GameManager.instance.playersAlive--;
            object data = new object[] { name, killCount , deathOrder};

            stateText.text = "You are Dead";

            RaiseEventOptions raiseEventOptions = new RaiseEventOptions
            {
                Receivers = ReceiverGroup.All,
                CachingOption = EventCaching.AddToRoomCache
            };
            SendOptions sendOptions = new SendOptions
            {
                Reliability = false
            };

            PhotonNetwork.RaiseEvent((byte)RaiseEventCode.DeadPlayerEventCode, data, raiseEventOptions, sendOptions);

            
        }
    }

    void OnDieEvent(EventData photonEvent)
    {
        if (photonEvent.Code == (byte)RaiseEventCode.DeadPlayerEventCode)
        {
            Debug.Log("Dead " + photonView.Owner.NickName);

            object[] data = (object[])photonEvent.CustomData;

            string nickNameOfDeadPlayer = (string)data[0];
            int totalKillCount = (int)data[1];
            deathOrder = (int)data[2];

            GameObject killCountUI = GameManager.instance.finalKillsUI[deathOrder - 1];

            killCountUI.GetComponent<TMP_Text>().text = deathOrder + ". " + nickNameOfDeadPlayer + ": " + totalKillCount + " kills";
            if (GameManager.instance.playersAlive == 0)
            {
                Debug.Log("All Players Dead");
                GameManager.instance.GameOver();
             
            }

            
        }
    }


}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    private InputHandler inputHandler;

    private Rigidbody rb;
    [SerializeField]
    private float moveSpeed;

    [SerializeField]
    private float rotateSpeed;

    [SerializeField]
    private bool rotateTowardsMouse;

    [SerializeField]
    public Camera camera;
    // Start is called before the first frame update
    void Start()
    {
        
        inputHandler = GetComponent<InputHandler>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        var targetVector = new Vector3(inputHandler.InputVector.x, 0, inputHandler.InputVector.y);

        var movementVector =  MoveTowardTarget(targetVector);
        if (!rotateTowardsMouse)
            RotateTowardMovementVector(movementVector);
        else
            RotateTowardMouseVector();
        //Move in the direction we are moving

    }

    private void RotateTowardMouseVector()
    {
        Ray ray = camera.ScreenPointToRay(inputHandler.MousePosition);
        
        if(Physics.Raycast(ray, out RaycastHit hitInfo, maxDistance: 300f))
        {
            var target = hitInfo.point;
            target.y = transform.position.y;
            transform.LookAt(target);
            

        }

    }

    private void RotateTowardMovementVector(Vector3 movementVector)
    { 
        if(movementVector.magnitude == 0) { return; };
        var rotation = Quaternion.LookRotation(movementVector);
        // transform.rotation = Quaternion.RotateTowards(transform.rotation, rotation, rotateSpeed);
       rb.MoveRotation(Quaternion.RotateTowards(transform.rotation, rotation, rotateSpeed));
    }

    private Vector3 MoveTowardTarget(Vector3 targetVector)
    {
        var speed = moveSpeed * Time.deltaTime;

        targetVector = Quaternion.Euler(0, camera.gameObject.transform.eulerAngles.y, 0) * targetVector;
        var targetPosition = transform.position + targetVector * speed;
        // transform.position = targetPosition;
        rb.MovePosition(targetPosition);
        return targetVector;
    }
}

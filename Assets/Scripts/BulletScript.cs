﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletScript : MonoBehaviour
{
    public float damage;
    public string playerOwner;
    
    private void OnTriggerEnter(Collider other)
    {
        if(other.TryGetComponent(out ZombieAI zombieAI) && !zombieAI.isDead)
        {
            Debug.Log("Hit zombie");
            zombieAI.TakeDamage(damage, playerOwner);
            Destroy(this.gameObject);
        }
    }
}

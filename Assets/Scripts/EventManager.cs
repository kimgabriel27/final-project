﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class EventManager : MonoBehaviour
{
    public static event Action<string> OnEnemyDeathEvent;
    public static event Action<string> OnWaveUpdate;

    public static void StartEnemyDeathEvent(string id)
    {
        OnEnemyDeathEvent?.Invoke(id);
    }

    public static void WaveUpdate(string id)
    {
        OnWaveUpdate?.Invoke(id);
    }
    
}
